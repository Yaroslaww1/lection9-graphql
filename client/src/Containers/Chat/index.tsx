import React, { useState, useEffect } from 'react';
import { MESSAGE_QUERY, MESSAGE_SUBSCRIPTION, REACTION_SUBSCRIPTION } from './queries';
import { useQuery, useSubscription } from '@apollo/react-hooks';
import Spinner from 'react-bootstrap/Spinner';
import MessageModal from '../MessageModal';
import { IMessage, IReaction } from '../../interfaces';
import Message from '../Message';
import MessageInput from '../MessageInput';
import { insertAndSort } from '../../helpers/sortHelper';
import FiltersComponent from '../../Components/FiltersComponent/index';

enum Filters {
  DATE = 'DATE',
  LIKES_COUNT = 'LIKES_COUNT',
  DISLIKES_COUNT = 'DISLIKES_COUNT'
}

interface IQueryVars {
  filter: Filters
}

const Chat: React.FC = () => {
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const [replyTo, setReplyTo] = useState<string>('');
  const [messages, setMessages] = useState<IMessage[]>([]);
  const [filter, setFilter] = useState<Filters>(Filters.DATE);

  const { loading: qLoading, error: qError, data: qData } = 
    useQuery<{messages: IMessage[]}, IQueryVars>
    (MESSAGE_QUERY, { variables: { filter } });
  useEffect(() => {
    if (!qError && !qLoading)
      setMessages(qData!.messages); 
  }, [qLoading, qError, qData]);


  const { loading: sMessageLoading, error: sMessageError, data: newMessage } = 
    useSubscription<{newMessage: IMessage}> (MESSAGE_SUBSCRIPTION);
  useEffect(() => {
    if (!sMessageError && !sMessageLoading) {
      const replyTo = newMessage?.newMessage.replyTo?.id;

      if (!replyTo)
        return setMessages([newMessage!.newMessage, ...messages]);

      const newMessages = insertAndSort(messages, newMessage!.newMessage);
      setMessages(newMessages);
    }
  }, [sMessageLoading, sMessageError, newMessage]);


  const { loading: sReactionLoading, error: sReactionError, data: newReaction } = 
    useSubscription<{newReaction: IReaction}> (REACTION_SUBSCRIPTION);
  useEffect(() => {
    if (!sReactionError && !sReactionLoading) {
      const newMessages = messages.map(message => {
        if (message.id === newReaction!.newReaction.message.id) {
          return {
            ...message,
            reactions: [...message.reactions, newReaction!.newReaction]
          }
        }
        return message;
      })
      setMessages(newMessages);
    }
  }, [sReactionLoading, sReactionError, newReaction]);

  if (qLoading)  return <Spinner animation="border" />
  if (qError) {
    console.error(qError);
    return <div>{qError}</div>
  }

  const onReply = (messageId: string) => {
    setIsModalOpen(true); 
    setReplyTo(messageId);
  }

  return (
    <div>
      <FiltersComponent
        onDateSort={() => setFilter(Filters.DATE)}
        onLikesSort={() => setFilter(Filters.LIKES_COUNT)}
        onDislikesSort={() => setFilter(Filters.DISLIKES_COUNT)}
      />
      {
        messages!.map(message => (
          <Message
            key={message.id}
            message={message}
            onReply={() => onReply(message.id)}
          />
        ))
      }
      <MessageInput/>
      <MessageModal 
        show={isModalOpen}
        onClose={() => setIsModalOpen(false)}
        replyTo={replyTo}
      />
    </div>
  )
}

export default Chat;