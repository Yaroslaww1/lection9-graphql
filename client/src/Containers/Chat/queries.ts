import gql from 'graphql-tag';

export const MESSAGE_QUERY = gql`
  query messageQuery($filter: Filters!) {
    messages(filter: $filter) {
      id
      content
      replies {
        id
        content
        reactions {
          id
          isLike
        }
      }
      reactions {
        id
        isLike
      }
      replyTo {
        id
      }
    }
  }
`;

export const MESSAGE_SUBSCRIPTION = gql`
  subscription newMessage {
    newMessage {
      id
      content
      replyTo {
        id
      }
      reactions {
        id
        isLike
      }
      replies {
        id
        content
        reactions {
          id
          isLike
        }
      }
    }
  }
`;

export const REACTION_SUBSCRIPTION = gql`
  subscription newReaction {
    newReaction {
      id
      isLike
      message {
        id
      }
    }
  }
`;