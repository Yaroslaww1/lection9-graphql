import React, { useState, useEffect } from 'react';
import { POST_REACTION_MUTATION } from './queries';
import { useMutation } from '@apollo/react-hooks';
import { Spinner } from 'react-bootstrap';
import { IMessage, IReaction } from '../../interfaces';

import './styles.css';

interface Props {
  message: IMessage
  onReply: () => void
};

const Message: React.FC<Props> = ({
  message,
  onReply
}) => {
  const {
    id,
    content,
    reactions,
    replyTo
  } = message;
  const replyId = replyTo?.id;

  const [saveReaction, { loading, error, data }] = useMutation<
    { saveReaction: IReaction },
    { messageId: string, isLike: boolean }
  > (POST_REACTION_MUTATION);

  if (loading)  return <Spinner animation="border" />
  if (error)  return <div>{error}</div>

  function getReactions(isLike: boolean): number {
    return reactions.filter(reaction => 
      reaction.isLike === isLike).length;
  }

  const onReaction = (isLike: boolean) => {
    saveReaction({ variables: { messageId: message.id, isLike } })
  }

  return (
    <div className={replyId ? 'reply' : ''}>
      {id} - {content} - {getReactions(true)} likes - {getReactions(false)} dislikes
      <button
        onClick={() => onReaction(true)}
      >
        Like
      </button>
      <button
        onClick={() => onReaction(false)}
      >
        Dislike
      </button>
      {
        !replyTo && 
          <button
            onClick={onReply}
          >
            Reply
          </button>
      }
    </div>
  )
}

export default Message;