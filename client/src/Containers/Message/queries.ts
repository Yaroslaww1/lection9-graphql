import gql from 'graphql-tag';

export const POST_REACTION_MUTATION = gql`
  mutation reactionMutation($messageId: String!, $isLike: Boolean!) {
    postReaction(messageId: $messageId, isLike: $isLike) {
      id
      isLike
    }
  }
`;