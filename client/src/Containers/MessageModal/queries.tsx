import gql from 'graphql-tag';

export const POST_MESSAGE_MUTATION = gql`
  mutation messageMutation($content: String!) {
    postMessage(content: $content) {
      id
    }
  }
`;