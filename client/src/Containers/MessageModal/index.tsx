import React, { useState } from 'react';
import { 
  Modal,
} from 'react-bootstrap';
import AddMessage from '../AddMessage';

interface Props {
  onClose: () => void
  show: boolean
  replyTo: string
}

const MessageModal: React.FC<Props> = ({
  show,
  onClose,
  replyTo
}) => {
  const [content, setContent] = useState<string>('');

  const onChange = (event: React.ChangeEvent<HTMLInputElement>) =>  {
    const { value } = event.currentTarget;
    setContent(value);
  }

  return (
    <Modal show={show} onHide={onClose}>
      <Modal.Header closeButton>
        <Modal.Title>Add message</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <input 
          value={content}
          onChange={onChange}
        />
      </Modal.Body>
      <Modal.Footer>
        <AddMessage 
          content={content}
          replyTo={replyTo}
        />
      </Modal.Footer>
    </Modal>
  );
}

export default MessageModal;