import React, { useState, useEffect } from 'react';
import MessageModal from '../MessageModal';
import Message from '../Message';
import AddMessage from '../AddMessage';

const MessageInput: React.FC = () => {
  const [text, setText] = useState<string>('');

  const onChange = (event: React.ChangeEvent<HTMLInputElement>) =>  {
    const { value } = event.currentTarget;
    setText(value);
  }

  return (
    <div>
      <input 
        placeholder="Enter your message here"
        value={text}
        onChange={onChange}
      />
      <AddMessage 
        content={text}
      />
    </div>
  )
}

export default MessageInput;