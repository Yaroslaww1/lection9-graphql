import gql from 'graphql-tag';

export const POST_MESSAGE_MUTATION = gql`
  mutation messageMutation($content: String!, $replyTo: String) {
    postMessage(content: $content, replyTo: $replyTo) {
      id
      content
      replyTo {
        id
      }
    }
  }
`;