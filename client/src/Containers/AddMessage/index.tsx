import React, { useState } from 'react';
import { POST_MESSAGE_MUTATION } from './queries';
import { useMutation } from '@apollo/react-hooks';
import {
  Button,
  Spinner
} from 'react-bootstrap';
import { IMessage } from '../../interfaces';

interface Props {
  content: string
  replyTo?: string
}

const AddMessage: React.FC<Props> = ({
  content,
  replyTo
}) => {
  const getVariables = () => {
    if (!replyTo)
      return { content }
    else
      return { content, replyTo }
  }
  const [saveMessage, { loading, error, data }] = useMutation<
    { saveMessage: IMessage },
    { content: string, replyTo?: string }
  >(POST_MESSAGE_MUTATION, {
    variables: getVariables()
  });

  const onSave = () => {
    saveMessage();
  }

  if (loading) return <Spinner animation="border" />
  if (error) return <div>{error}</div>

  return (
    <>
      {
        content
          ? <Button variant="primary" onClick={onSave}>
              Send
            </Button>
          : <Button variant="primary" >
              Message body cant be empty
            </Button>
      }
    </>
  );
}

export default AddMessage;