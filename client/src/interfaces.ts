export interface IReaction {
  id: string
  isLike: boolean
  message: IMessage
}

export interface IMessage {
  id: string
  content: string
  reactions: IReaction[]
  replies: IMessage[]
  replyTo?: {
    id: string
  }
}
