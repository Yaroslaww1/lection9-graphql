import { IMessage } from "../interfaces";

export function insertAndSort(messages: IMessage[], newMessage: IMessage): IMessage[] {
  const replyTo = newMessage.replyTo?.id;
  let _messages = messages;
  // update replies array for all message
  _messages = _messages.map(message => {
    if (message.id === replyTo) {
      return {
        ...message,
        replies: [newMessage, ...message.replies]
      }
    }
    return message;
  })
  _messages.push(newMessage);
  const newMessages: IMessage[] = [];
  for (const message of _messages) {
    if (!message.replyTo) {
      newMessages.push(message);
      const replies = message.replies.map(reply => ({
        ...reply,
        replyTo: { id: message.id },
      })); 
      newMessages.push(...replies);
    }
  }

  return newMessages;
}