import React from 'react';

export interface Props {
  onDateSort: () => void
  onLikesSort: () => void
  onDislikesSort: () => void
} 

const FiltersComponent: React.FC<Props> = ({
  onDateSort,
  onLikesSort,
  onDislikesSort
}) => {
  return (
    <div>
      <button
        onClick={onDateSort}
      >
        Sort by date
      </button>
      <button
        onClick={onLikesSort}
      >
        Sort by likes
      </button>
      <button
        onClick={onDislikesSort}
      >
        Sort by dislikes
      </button>
    </div>
  )
}

export default FiltersComponent;