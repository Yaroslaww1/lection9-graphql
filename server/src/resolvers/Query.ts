/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { Prisma } from "../generated/prisma-client";

interface IReaction {
  id: string
  isLike: boolean
}

interface IMessage {
  id: string
  content: string
  createdAt: string
  reactions: IReaction[]
  replies: IMessage[]
  replyTo: IMessage
}

enum Filters {
  DATE = 'DATE',
  LIKES_COUNT = 'LIKES_COUNT',
  DISLIKES_COUNT = 'DISLIKES_COUNT'
}

export async function reactions(parent: any, args: { filter: Filters }, context: { prisma: Prisma }) {
  return context.prisma.message({
    id: parent.id
  }).reactions()
}

export async function messages(parent: any, args: any, context: { prisma: Prisma }) {
  const messages: IMessage[] = await context.prisma.messages().$fragment(`
    fragment F1 on Message {
      id
      content
      createdAt
      reactions {
        id
        isLike
      }
      replies {
        id
        content
        reactions {
          id
          isLike
        } 
        replies {
          id
          content
        }
        replyTo {
          id
        }
      }
      replyTo {
        id
      }
    }
  `);

  const newMessages: IMessage[] = [];
  for (const message of messages) {
    if (!message.replyTo) {
      newMessages.push(message);
    }
  }

  const byDate = (m1: IMessage, m2: IMessage) => {
    const date1 = new Date(m1.createdAt);
    const date2 = new Date(m2.createdAt);
    return date2.getTime() - date1.getTime();
  }

  const byLikes =  (m1: IMessage, m2: IMessage) => {
    const m1LikesCount = m1.reactions.filter(reaction => reaction.isLike).length;
    const m2LikesCount = m2.reactions.filter(reaction => reaction.isLike).length;
    return m2LikesCount - m1LikesCount;
  }

  const byDislikes =  (m1: IMessage, m2: IMessage) => {
    const m1DislikesCount = m1.reactions.filter(reaction => !reaction.isLike).length;
    const m2DislikesCount = m2.reactions.filter(reaction => !reaction.isLike).length;
    return m2DislikesCount - m1DislikesCount;
  }

  switch (args.filter) {
    case Filters.DATE: { newMessages.sort(byDate); break; }
    case Filters.LIKES_COUNT: { newMessages.sort(byLikes); break; }
    case Filters.DISLIKES_COUNT: { newMessages.sort(byDislikes); break; }
  }

  const newMessagesSorted: IMessage[] = [];
  for (const message of newMessages) {
    if (!message.replyTo) {
      newMessagesSorted.push(message);
      const replies = message.replies.map(reply => ({
        ...reply
      }));
      newMessagesSorted.push(...replies);
    }
  }

  return newMessagesSorted;
}