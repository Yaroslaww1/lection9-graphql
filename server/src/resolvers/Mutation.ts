/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { Prisma } from "../generated/prisma-client";
import shortid from 'shortid';

interface IPostMessageArgs {
  content: string
  replyTo?: string
}

export async function postMessage(parent: any, args: IPostMessageArgs, context: { prisma: Prisma }) {
  console.log(args.replyTo);
  if (args.replyTo) {
    const replyToMessageExists = await context.prisma.$exists.message({
      id: args.replyTo
    });

    if (!replyToMessageExists)
      throw new Error(`Message with id: ${args.replyTo} was not found`);

    const message = await context.prisma.createMessage({
      id: shortid.generate(),
      content: args.content,
      replyTo: { connect: { id: args.replyTo } }
    }).$fragment(`
      fragment F4 on Message {
        id
        content
        reactions {
          id
          isLike
        }
        replies {
          id
          content
          reactions {
            id
            isLike
          }
        }
        replyTo {
          id
        }
      }
    `);

    console.log('CREATED');

    return message;
  }

  const message = await context.prisma.createMessage({
    id: shortid.generate(),
    content: args.content
  });

  return message;
}

interface IPostReactionArgs {
  messageId: string
  isLike: boolean
}

export async function postReaction(parent: any, args: IPostReactionArgs, context: { prisma: Prisma }) {
  const messageExists = await context.prisma.$exists.message({
    id: args.messageId
  });

  if (!messageExists)
    throw new Error(`Message with id: ${args.messageId} was not found`);

  const reaction = await context.prisma.createReaction({
    id: shortid.generate(),
    isLike: args.isLike,
    message: { connect: { id: args.messageId } }
  });

  return reaction;
}