/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { Prisma } from "../generated/prisma-client";

async function newMessageSubscribe(parent: any, args: any, context: { prisma: Prisma }) {
  const message = context.prisma.$subscribe.message({
    mutation_in: ['CREATED']
  }).node().$fragment(`
    fragment F2 on Message {
      id
      content
      reactions {
        id
        isLike
      }
      replies {
        id
        content
        reactions {
          id
          isLike
        }
        replyTo {
          id
        }
      }
      replyTo {
        id
      }
    }
  `);

  return message;
}

export const newMessage = {
  subscribe: newMessageSubscribe,
  resolve: (payload: any) => {
    console.log('payload', payload);
    return payload;
  }
}

async function newReactionSubscribe(parent: any, args: any, context: { prisma: Prisma }) {
  const reaction =  context.prisma.$subscribe.reaction({
    mutation_in: ['CREATED']
  }).node().$fragment(`
    fragment F3 on Reaction {
      id
      isLike
      message {
        id
      }
    }
  `);
  
  return reaction;
}

export const newReaction = {
  subscribe: newReactionSubscribe,
  resolve: (payload: any) => payload
}


