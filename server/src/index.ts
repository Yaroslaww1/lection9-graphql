import { GraphQLServer } from 'graphql-yoga';
import { prisma } from './generated/prisma-client';
import * as queryResolvers from './resolvers/Query';
import * as mutationResolvers from './resolvers/Mutation';
import * as subscriptionResolvers from './resolvers/Subscription';

const resolvers = {
  Query: {
    ...queryResolvers
  },
  Mutation: {
    ...mutationResolvers
  },
  Subscription: {
    ...subscriptionResolvers
  }
}

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  context: { prisma }
});

server.start(() => {
  console.log('http://localhost:4000')
})